<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {
      function __construct() {
          parent::__construct();
      }
      function signin_with_twitter($userInfos){
          //Check USer OauthID;
          $uid = $userInfos['uid'];
          $query = $this->db->get_where('users',array('uid' => $uid));
          
          
          if($query->num_rows()>0){
              
              //Check the UserStatus...
              $curr_user_info = $query->result_array();
              if ($curr_user_info['active_status'] == FALSE){
                  return 0;
              }
              //Already Registered Account; and Update Account Info...
              $this->db->update('users',$userInfos,array('uid' => $uid));
              return 1;
          }
          else {
              $this->db->update('users',$userInfos);
              return 1;
          }
      } 
}