<!DOCTYPE html>
<html lang="en">
    <head>

        <title>Welcome</title>

        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <!-- Main Font -->
        <script src="<?php echo base_url();?>assets/js/webfontloader.min.js"></script>

        <script>
            WebFont.load({
                google: {
                    families: ['Roboto:300,400,500,700:latin']
                }
            });
        </script>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/Bootstrap/dist/css/bootstrap-reboot.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/Bootstrap/dist/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/Bootstrap/dist/css/bootstrap-grid.css">

        <!-- Main Styles CSS -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/main.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/fonts.min.css">



    </head>

    <body 
        <?php if($page_name == "landing"){ ?> class = "landing-page" <?php }
                else if ($page_name == "main") { ?> class = "page-has-left-panels page-has-right-panels" <?php } ?>
    >

        <?php include $page_name.'.php';?>

        <!-- JS Scripts -->
        <script src="<?php echo base_url();?>assets/js/jquery-3.2.1.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.appear.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.mousewheel.js"></script>
        <script src="<?php echo base_url();?>assets/js/perfect-scrollbar.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.matchHeight.js"></script>
        <script src="<?php echo base_url();?>assets/js/svgxuse.js"></script>
        <script src="<?php echo base_url();?>assets/js/imagesloaded.pkgd.js"></script>
        <script src="<?php echo base_url();?>assets/js/Headroom.js"></script>
        <script src="<?php echo base_url();?>assets/js/velocity.js"></script>
        <script src="<?php echo base_url();?>assets/js/ScrollMagic.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.waypoints.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.countTo.js"></script>
        <script src="<?php echo base_url();?>assets/js/popper.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/material.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap-select.js"></script>
        <script src="<?php echo base_url();?>assets/js/smooth-scroll.js"></script>
        <script src="<?php echo base_url();?>assets/js/selectize.js"></script>
        <script src="<?php echo base_url();?>assets/js/swiper.jquery.js"></script>
        <script src="<?php echo base_url();?>assets/js/moment.js"></script>
        <script src="<?php echo base_url();?>assets/js/daterangepicker.js"></script>
        <script src="<?php echo base_url();?>assets/js/simplecalendar.js"></script>
        <script src="<?php echo base_url();?>assets/js/fullcalendar.js"></script>
        <script src="<?php echo base_url();?>assets/js/isotope.pkgd.js"></script>
        <script src="<?php echo base_url();?>assets/js/ajax-pagination.js"></script>
        <script src="<?php echo base_url();?>assets/js/Chart.js"></script>
        <script src="<?php echo base_url();?>assets/js/chartjs-plugin-deferred.js"></script>
        <script src="<?php echo base_url();?>assets/js/circle-progress.js"></script>
        <script src="<?php echo base_url();?>assets/js/loader.js"></script>
        <script src="<?php echo base_url();?>assets/js/run-chart.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.magnific-popup.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.gifplayer.js"></script>
        <script src="<?php echo base_url();?>assets/js/mediaelement-and-player.js"></script>
        <script src="<?php echo base_url();?>assets/js/mediaelement-playlist-plugin.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/ion.rangeSlider.js"></script>

        <script src="<?php echo base_url();?>assets/js/base-init.js"></script>
        <script defer src="<?php echo base_url();?>assets/fonts/fontawesome-all.js"></script>

        <script src="<?php echo base_url();?>assets/Bootstrap/dist/js/bootstrap.bundle.js"></script>

    </body>
</html>