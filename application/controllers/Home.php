<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
	}
	public function index()
	{	
		$page_data['page_name'] = 'main';
		$page_data['page_title'] = 'Welcome';
		$this->load->view('frontend/index',$page_data);
	}

	public function twitterAuth(){
		
		$this->twitterauth->getLogin();
	}

	public function twitterResponse(){
		$twitter_data = $this->twitterauth->checkAuth();
		$twitter_data_array = json_decode(json_encode($twitter_data), TRUE);

		$userInfos = array(
			'uid' => $twitter_data_array['id'],
			'username' => $twitter_data_array['screen_name'],
			'locale' => $twitter_data_array['lang'],
			'profile_url' => 'https://twitter.com/'.$twitter_data_array['screen_name'],
			'picture_url' => $twitter_data_array['profile_image_url'],
			'active_status' => TRUE
		);

		$signup_result = $this->user_model->signin_with_twitter($userInfos);
		if ($signup_result == 1){
			$page_data['page_name'] = 'main';
			$page_data['page_title'] = 'Top Ten Hashtags & Tweets';
			$this->load->view('frontend/index',$page_data);
		} else {
			//return 404 Page or Blocked Account Page for some reasons....
		}
		
	}
	
}
