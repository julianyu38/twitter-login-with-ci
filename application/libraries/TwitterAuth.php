<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once APPPATH.'third_party/vendor/abraham/twitteroauth/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;
 
class TwitterAuth
{
 
	protected $CI;
 
    public function __construct()
    {
 
    	$this->CI =& get_instance();
        $this->CI->load->library('session');        
        $this->connection= new TwitterOAuth(Consumer_Key, Consumer_Secret);

    }
    public function getLogin(){
 
    	$this->request_token = $this->connection->oauth("oauth/request_token", array("oauth_callback" => base_url()."home/twitterResponse"));
    	// print_r($this->request_token);
 
        $_SESSION['oauth_token'] = $this->request_token['oauth_token'];
        $_SESSION['oauth_token_secret'] = $this->request_token['oauth_token_secret'];
        $url = $this->connection->url("oauth/authorize", array("oauth_token" => $this->request_token['oauth_token']));
 
        $this->CI->session->set_userdata('oauth_token', $this->request_token['oauth_token']);
		$this->CI->session->set_userdata('oauth_token_secret',$this->request_token['oauth_token_secret']);
    	
    	redirect($this->connection->url('oauth/authorize', array('oauth_token' => $this->request_token['oauth_token']), 'refresh'));
 
    }
    public function checkAuth(){
 
    	$this->connection = new TwitterOAuth(Consumer_Key, Consumer_Secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
 
		$this->access_token = $this->connection->oauth('oauth/access_token', array('oauth_verifier' => $_REQUEST['oauth_verifier'], 'oauth_token'=> $_GET['oauth_token']));
 
	    $this->connection = new TwitterOAuth(Consumer_Key, Consumer_Secret, $this->access_token['oauth_token'], $this->access_token['oauth_token_secret']);
 
	    $this->user_info = $this->connection->get('account/verify_credentials');
 
	    return $this->user_info;
 
    }
}